# RandomPageNames Module

This module helps you create random pagenames that are unique across your site.
You can also define a selector to define a subset of pages where your pagename
should be unique. By default pagenames are unique globally.

Usage example in `/site/ready.php`:

```php
// when a species was added make a globally unique pagename with 5 digits
$wire->addHookAfter('Pages::added', function($event) {
  $page = $event->arguments(0);
  if($page->template != 'species') return;

  $page->setRandomPageName([
    'minLength' => 5,
    'maxLength' => 5,
  ]);
});
```

```php
// when a animal was added make a unique pagename with 3 digits under the current page parent
$wire->addHookAfter('Pages::added', function($event) {
  $page = $event->arguments(0);
  if($page->template != 'animal') return;

  $page->setRandomPageName("has_parent={$page->parent}", [
    'minLength' => 3,
    'maxLength' => 3,
  ]);
});
```

This could result in such a pagetree:

```
/
/11111 (species)
/11111/aaa (animal)
/11111/bbb (animal)
/22222 (species)
/22222/aaa (animal)
/22222/bbb (animal)
```

## Parameter explanation

You can define three parameters for the function. The parameters are distinguished
only by type, not by order (you can leave them empty or mix them as you want):

* Integer: This will be taken as the page name length
* String: This will be taken as the subset page selector
* Array: This will be taken as the options array for the string generation

```php
$page->getRandomPageName(5); // 5uj88
$page->getRandomPageName(5, [
  'minDigits' => 2
]); // o6tn6
```